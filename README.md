# Welcome
This wiki provides an overview of the `org.ambientdynamix.contextplugins.logger` plug-in for the [Dynamix Framework](http://ambientdynamix.org). This plug-in allows apps to easily find the call history in the mobile phone with details such as the number, date, time, call type and duration.

Note that this guide assumes an understanding of the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/).

# Plug-in Requirements
* Android API level 11 and higher (Android Platform 3.0 and higher)
* Dynamix version 2.1 and higher


# Context Support
* `org.ambientdynamix.contextplugins.logger.callhistory` reads information about the call log in the phone.

# Native App Usage
Integrate the Dynamix Framework within your app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your native programs, see [this guide](http://ambientdynamix.org/documentation/native-app-quickstart).

## Add context support for the plug-in.
```
#!java
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.logger", "org.ambientdynamix.contextplugins.logger.callhistory", listener,callback );
```
## Handle Events
Once context support is added for `org.ambientdynamix.contextplugins.logger.callhistory`, the supplied listener will start receiving events. Events implement the Dynamix `IContextInfo` interface, which provides a variety of general event metadata (e.g., the event time, source and expiry information). The events are of type `IPhoneCallsHistory`, which includes the following additional data:

1. `getCallLog()`: Returns a String. The entire call history currently accessible is provided as a block of string here. The details include; Phone Number, Call Type (i.e.INCOMING ), Call Date (i.e. Thu Sep 03 00:27:46 GMT+08:00 2015) and Call duration in seconds
2. `getCallHistory()`: Returns a ArrayList. Each entry in the arraylist is a call made/received or missed, along with the additional details mentioned above. 

## Interact with plugin
No need for any configuration bundles. Both handleContextRequest() and handleConfiguredContextRequest() behaves in a similar way. 

Note that the following methods return data using a Dynamix `callback`, as described [here](http://ambientdynamix.org/documentation/integration/).

# Web App Usage
TODO

## Add context support for the plug-in.
TODO

## Handle Events
TODO

## Interact with the plugin
TODO